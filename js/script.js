let options = ["pedra", "papel", "tesoura"]



function captureOptions() {
  let takeOptions = document.querySelectorAll(".options div")
  takeOptions.forEach(element => {
    element.onclick = function () {
      switch (this.className) {
        case "pedra":
          compare("pedra")
          break
        case "papel":
          compare("papel")
          break
        case "tesoura":
          compare("tesoura")
          break
      }
    }
  })
}

function compare(name) {

  let random = options[Math.floor(Math.random() * 3)]
  let computer = document.querySelector(".computer div")
  let h3 = document.querySelector("h3")
  let img = document.querySelector(".computer img")

  img.src = `./img/${random}.png`
  computer.appendChild(img)


  h3.innerText = ""




  if (name === random) {
    h3.innerText = "LOL, voce empatou. Nao me diga que..."
    h3.classList.add("drawn")
  }
  else if (name === 'tesoura' && random === "papel") {
    h3.innerText = "Vou te denunciar"
    h3.classList.remove("drawn")
    h3.classList.remove("lose")
    h3.classList.add("victory")
  }
  else if (name === 'papel' && random === "tesoura") {
    h3.innerText = "Voce perdeu parceiro. Vai um biscoitin?"
    h3.classList.remove("drawn")
    h3.classList.remove("victory")
    h3.classList.add("lose")

  } else if (name === 'pedra' && random === "tesoura") {
    h3.innerText = "Como assim? voce deve ser algum hacker"
    h3.classList.remove("drawn")
    h3.classList.remove("lose")
    h3.classList.add("victory")
  }
  else if (name === "tesoura" && random === "pedra") {
    h3.innerText = "Chora agora nao, guarda a lagrima pra usar nas proximas derrotas"
    h3.classList.remove("drawn")
    h3.classList.remove("victory")
    h3.classList.add("lose")
  }
  else if (name === "papel" && random === "pedra") {
    h3.innerText = "Vou parar de jogar com voce"
    h3.classList.remove("drawn")
    h3.classList.remove("lose")
    h3.classList.add("victory")
  }
  else if (name === "pedra" && random === "papel") {
    h3.innerText = "so jogo pra ganhar"
    h3.classList.remove("drawn")
    h3.classList.remove("victory")
    h3.classList.add("lose")
  }
}




captureOptions()